require "rvm/capistrano"
require "bundler/capistrano"

set :deploy_to, "/home/myplnr/myplnr"
set :repository, "git@bitbucket.org:muamer/myplnr.git"
set :rvm_ruby_string, "2.0.0@myplnr"
set :use_sudo, false
set :deploy_via, :remote_cache

set :user, "myplnr"

role :web, "162.243.34.32"
role :app, "162.243.34.32"
role :db,  "162.243.34.32", :primary => true

after "deploy:restart", "deploy:cleanup"
after "deploy", "rvm:trust_rvmrc"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :web, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end
end

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end
