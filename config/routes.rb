Myplnr::Application.routes.draw do
  root 'myplnr#index'
  namespace :api do
    namespace :v1 do
      resources :lists, only: [:show, :create] do
        resources :tasks, only: [:create, :update, :destroy]
        post :quadrant
      end
    end
  end
end
