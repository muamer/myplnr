class CreateList < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :key, length: 8
      t.timestamps
    end
  end
end
