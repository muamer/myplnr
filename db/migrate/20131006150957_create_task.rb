class CreateTask < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :content, null: false
      t.integer :quadrant, null: false, default: 1
      t.integer :index, null: false
      t.boolean :completed, null: false, default: false
      t.integer :list_id, null: false
      t.timestamps
    end
  end
end
