class Task < ActiveRecord::Base
  validates :content, presence: true
  validates :quadrant, presence: true
  validates :index, presence: true
  validates :list, presence: true

  belongs_to :list
end
