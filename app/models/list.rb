class List < ActiveRecord::Base
  validates :key, uniqueness: true
  before_create :generate_key

  has_many :tasks

  def generate_key
    self.key = Digest::SHA1.hexdigest(Time.now.to_s + Random.rand.to_s).slice(0..7)
  end
end
