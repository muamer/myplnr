class Api::V1::TasksController < Api::V1::BaseController
  def create
    list = List.find_by_key(params[:list_id])

    render json: list.tasks.create(task_params)
  end

  def update
    list = List.find_by_key(params[:list_id])
    task = list.tasks.find(params[:task][:id])
    task.update_attributes(task_params)

    render json: task
  end

  def destroy
    list = List.find_by_key(params[:list_id])
    task = list.tasks.find(params[:id])
    render json: task.destroy
  end

  private

  def task_params
    params.require(:task).permit(:id, :content, :quadrant, :index, :completed)
  end
end
