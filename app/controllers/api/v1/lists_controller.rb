class Api::V1::ListsController < Api::V1::BaseController
  def create
    render json: List.create()
  end

  def show
    render json: List.find_by_key(params[:id])
  end

  def quadrant
    quadrant_index = params[:quadrant][:index]

    params[:quadrant][:tasks].each_with_index do |task_id, index|
      Task.update_all({index: index, quadrant: quadrant_index}, id: task_id)
    end

    render json: {}
  end
end
