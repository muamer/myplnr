angular.module('ui.sortable').value('uiSortableConfig', {
  sortable: {
    connectWith: '.sortable',

    update: function(e, ui) {
      $(e.target).attr('data-dirty', true);
    },

    // have to update order on sort stop because of race condition
    // with angular update and dom update.
    stop: function(e, ui) {
      $('.quadrant[data-dirty=true]').each(function(index, quadrantElement){
        var quadrantScope = $(quadrantElement).scope();
        var quadrantIndex = quadrantScope.quadrant;

        var tasks = $(quadrantElement).find('.task').map(function(index, taskElement){
          var task = $(taskElement).scope().task;
          task.index = index;
          task.quadrant = quadrantIndex;
          return task.id;
        });

        var quadrant = {
          index: quadrantIndex,
          tasks: tasks.toArray()
        }
        
        if(tasks.length)
          quadrantScope.updateQuadrant(quadrant);

        $(quadrantElement).attr('dirty', false);
      });
    }
  }
});
