myplnr.directive('taskFocus', ['$timeout', function taskFocus($timeout) {
  return function(scope, element, attrs) {
    scope.$watch(attrs.taskFocus, function(value) {
      if (value) {
        $timeout(function() {
          element[0].focus();
        });
      }
    });
  };
}]);
