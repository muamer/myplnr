myplnr.directive('taskEscape', function(){
  return function(scope, element, attrs) {
    element.bind('keydown', function(e) {
      if(e.keyCode === 27)
        scope.$apply(attrs.taskEscape);
    });
  };
});
