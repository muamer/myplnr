myplnr.directive('fullHeight', function($timeout){
  return function($scope, element, attrs) {
    $scope.$on('tasksLoaded', function(){
      $timeout(function(){
        newHeight = $(element).height() - $('#topbar').height();

        $(element)
          .css('height', newHeight)
          .css('min-height', newHeight);
      });  
    }); 
  };
});
