myplnr.directive('taskBlur', function(){
  return function(scope, element, attrs) {
    element.bind('blur', function(){
      console.log('blur')
      scope.$apply(attrs.taskBlur);
    });
  };
});
