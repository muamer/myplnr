myplnr.controller('homeController', ['$scope', '$location', 'listFactory', function($scope, $location, listFactory){
  $scope.createList = function() {
    listFactory.createList()
      .then(function(response){
        console.log('created list ', response.data.list);
        $location.path('/' + response.data.list.key); 
      })
      .then(undefined, function(error){
        console.log('ups. something went wrong', error);
      });
  };
}]);
