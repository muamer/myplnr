myplnr.controller('tasksController', ['$scope', '$routeParams', 'listFactory', 'taskFactory', function($scope, $routeParams, listFactory, taskFactory) {
  $scope.quadrants = [1, 2, 3, 4];
  $scope.$emit('tasksLoaded');

  listFactory.getList($routeParams.listKey)
    .then(function(response){
      $scope.list = response.data.list;
      $scope.tasks = response.data.list.tasks;
    })
    .then(undefined, function(){
      console.log('Cannot fetch list', $routeParams.listId);
    });

  $scope.newTask = taskFactory.setupNewTask();

  // global editing flag. It is true if any task is in editing state
  $scope.isEditing = false;

  $scope.selectQuadrant = function(quadrant) {
    $scope.newTask.quadrant = quadrant;
  };

  $scope.saveTask = function() {
    if($scope.parentForm.newTaskForm.$invalid) 
      return;

    var task = $scope.newTask;
    $scope.tasks.push(task);
    var currentQuadrant = $scope.newTask.quadrant;
    $scope.newTask = taskFactory.setupNewTask({quadrant: currentQuadrant})

    taskFactory.createTask($scope.list.key, task)
      .then(function(response){
        angular.extend(task, response.data.task);
      })
      .then(undefined, function(error) {
        console.log('Cannot create task', error);
      })
  };

  $scope.editTask = function(task) {
    $scope.isEditing = true;
    $scope.editedTask = task;
    $scope.originalTask = angular.extend({}, task);
  };

  $scope.doneEditing = function(task) {
    if($scope.parentForm.editTaskForm.$invalid) 
      return;

    $scope.isEditing = false;
    $scope.editedTask = null;
    task.content = task.content.trim();

    taskFactory.updateTask($scope.list.key, task)
      .then(undefined, function(error) {
        console.log('Cannot update task', error);
      });
  };

  $scope.cancelEditing = function(task) {
    console.log('cancel editing');
    $scope.tasks[$scope.tasks.indexOf(task)] = $scope.originalTask;
    $scope.isEditing = false;
    $scope.editedTask = null;
  };

  $scope.toggleComplete = function(task) {
    console.log('toggle complete task, persist this change.', task);
    taskFactory.updateTask($scope.list.key, task)
      .then(undefined, function(error) {
        console.log('Cannot update task', error);
      });
  };

  $scope.deleteTask = function(task) {
    taskFactory.deleteTask($scope.list.key, task)
      .then(function(response){
        $scope.isEditing = false;
        $scope.tasks.splice($scope.tasks.indexOf(task), 1);
      })
      .then(undefined, function(error){
        console.log('Cannot delete task', error);
      });
  };

  $scope.updateQuadrant = function(quadrant) {
    listFactory.updateQuadrant($scope.list.key, quadrant)
      .then(undefined, function(error){
        console.log('Cannot update quadrant', error);
      })
  };
}]);
