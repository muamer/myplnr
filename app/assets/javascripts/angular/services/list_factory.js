myplnr.factory('listFactory', ['$http', function($http) {
  var baseUrl = 'api/v1/lists/';
  var listFactory = {};

  listFactory.createList = function() {
    return $http.post(baseUrl, {})
  };

  listFactory.getList = function(id) {
    return $http.get(baseUrl + id)
  };

  listFactory.updateQuadrant = function(listId, quadrant) {
    return $http.post(baseUrl + listId + '/quadrant', {quadrant: quadrant});
  };

  return listFactory;
}]);
