myplnr.factory('taskFactory', ['$http', function($http) {
  var baseUrl = 'api/v1/lists/';
  var taskFactory = {};

  taskFactory.setupNewTask = function(task) {
    task = typeof task !== 'undefined' ? task : {};

    var newTask = {
      content: '',
      quadrant: 2,
      index: Math.round((new Date()).getTime() / 1000),
      completed: false
    };

    return angular.extend(newTask, task);
  };

  taskFactory.createTask = function(listId, task) {
    return $http.post(baseUrl + listId + '/tasks', task);
  };

  taskFactory.updateTask = function(listId, task) {
    return $http.put(baseUrl + listId + '/tasks/' + task.id, task);
  };

  taskFactory.deleteTask = function(listId, task) {
    console.log(listId, task);
    return $http.delete(baseUrl + listId + '/tasks/' + task.id);
  };

  return taskFactory;
}]);
