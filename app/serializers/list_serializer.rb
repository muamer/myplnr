class ListSerializer < ActiveModel::Serializer
  attributes :key
  has_many :tasks, embed: :objects
end
