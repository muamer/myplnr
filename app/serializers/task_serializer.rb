class TaskSerializer < ActiveModel::Serializer
  attributes :id, :content, :quadrant, :index, :completed
end
